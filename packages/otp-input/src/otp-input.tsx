import React, { ComponentType, createElement, FC, useMemo, useRef, useState } from 'react';

interface InputProps {
  name?: string
  value?: string
  onChange?: React.ChangeEventHandler<HTMLInputElement>
  onFocus?: React.FocusEventHandler<HTMLInputElement>
}

interface OtpInputProps {
  InputComponent?: ComponentType<InputProps> | 'input'
  ContainerComponent?: ComponentType | keyof JSX.IntrinsicElements
  length?: number
  name?: string
}

const findNextElement = (el: ChildNode, selector: string) => {
  while (el.nextSibling) {
    el = el.nextSibling
    if (el instanceof HTMLElement && el.matches(selector)) {
      return el
    }
  }
  return null;
}

const findPrevElement = (el: ChildNode, selector: string) => {
  while (el.previousSibling) {
    el = el.previousSibling
    if (el instanceof HTMLElement && el.matches(selector)) {
      return el
    }
  }
  return null;
}

const splitFirstString = (s: string) => [s.charAt(0), s.slice(1)]

export function OtpInput({
  InputComponent = 'input',
  ContainerComponent = 'div',
  length = 6,
  name,
}: OtpInputProps) {
  const [values, setValues] = useState<Record<number, string>>({})
  const refs = useRef<HTMLElement[]>([])
  const id = useMemo(() => Math.random() + '@' + Date.now(), [])
  const inputEmptyArray = useMemo(() => Array(length).fill(length).map((_, i) => i), [length])
  const memoizedName = useMemo(() => name || id, [name, id])
  const createName = (index: number) => `${memoizedName}[${index}]`
  const goToNextInput = (currentIndex: number, remainValue: number) => {
    if (currentIndex + 1 < length) {

    }
  }

  const createOnFocus = (index: number): React.FocusEventHandler<HTMLInputElement> => (e) => {
    const input = e.currentTarget;
    input.setSelectionRange(0, input.value.length)
  }

  const createOnChange = (index: number): React.ChangeEventHandler<HTMLInputElement> => (e) => {
    const input = e.currentTarget;
    if (!input.value) {
      setValues(values => ({
        ...values,
        [index]: ''
      }))
      let prevElement = findPrevElement(input, 'input');
      prevElement && prevElement.focus()
      return;
    }
    let nextInput = findNextElement(input, 'input');
    setValues(values => {
      console.log(1, values);
      const newValues = { ...values }
      let [value, remainValue] = splitFirstString(input.value)
      newValues[index] = value;
      input.setSelectionRange(0, input.value.length)
      let nextIndex = index
      while (nextInput && remainValue) {
        [value, remainValue] = splitFirstString(remainValue)
        nextIndex += 1;
        newValues[nextIndex] = value;
        nextInput?.focus()
        nextInput = findNextElement(nextInput, 'input')
      }
      return newValues
    })
    if (nextInput) {
      nextInput.focus()
    }
  }

  return (
    <ContainerComponent>
      {
        inputEmptyArray.map(index => (
          <InputComponent
            key={index}
            onFocus={createOnFocus(index)}
            name={createName(index)}
            onChange={createOnChange(index)}
            value={values[index] || ''}
          />
        ))
      }
    </ContainerComponent>
  )
}
