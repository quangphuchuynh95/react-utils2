import { defaultLibConfig } from '../../vite.config.lib'
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react'

export default function getConfig() {
  return defineConfig({
    ...defaultLibConfig,
    plugins: [
      react()
    ]
  })
}
