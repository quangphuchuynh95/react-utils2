import { OtpInput } from '@react-utils2/otp-input'

function App() {
  return (
    <div className="App">
      <OtpInput />
    </div>
  )
}

export default App
