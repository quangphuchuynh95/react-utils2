import { defineConfig, InlineConfig } from 'vite'
import { ModuleFormat } from 'rollup';

export const defaultLibConfig: InlineConfig = {
  build: {
    outDir: './lib',
    lib: {
      entry: './src/lib.ts',
      formats: ['es', 'cjs'],
      fileName: (format) => `lib.${format}.js`,
    },
    rollupOptions: {
      external: [
        'react',
        'react-dom',
      ]
    }
  }
}
